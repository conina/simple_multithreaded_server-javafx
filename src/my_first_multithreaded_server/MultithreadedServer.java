package my_first_multithreaded_server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MultithreadedServer {

	private static final int PORT = 2001;
	private static final int MAX_CLIENTS = 10;
	private static ExecutorService client_pool = null;
	private static ServerSocket listening_socket = null;;	

	public static class SingleConnectionHandler implements Runnable {

		private DataInputStream in_stream = null;
		private DataOutputStream out_stream = null;
		private final Socket c_socket;
		private Thread client_thread = null;
		
		//constructor
		public SingleConnectionHandler(Socket client_socket){
			this.c_socket = client_socket;
		}

		@Override
		public void run() {

			// creating input and output streams
			try {
				in_stream = new DataInputStream(c_socket.getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				out_stream = new DataOutputStream(c_socket.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}

			this.client_thread = Thread.currentThread();
			System.out.println("Input/Output streams established for thread: " + this.client_thread );

			Scanner client_message_scanner = new Scanner(in_stream);
			PrintWriter to_client_writer = new PrintWriter(new OutputStreamWriter(out_stream), true);

			to_client_writer.println("Welcome to the dream world");

			String user_input = "";

			//defining the communication between server and client
			while (!user_input.equalsIgnoreCase("over")) {
				user_input = client_message_scanner.nextLine();
				to_client_writer.println(user_input.toUpperCase());
				System.out.println(user_input);
			}

			System.out.println("Connection closed for the thread: " + this.client_thread);

			try {
				c_socket.close();
				out_stream.close();
				in_stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main (String[] arg) {

		System.out.println("Opening server...");

		try {
			// listening socket is waiting for new clients
			listening_socket = new ServerSocket(PORT);	
			System.out.println("Listening for new clients...");

			while (true) {

				Socket client_socket = listening_socket.accept();

				System.out.println("New client accepted...");

				//creating a thread pool for handling multiple clients
				client_pool = Executors.newFixedThreadPool(MAX_CLIENTS);

				//opening a new thread for one client
				client_pool.submit(new SingleConnectionHandler(client_socket));
			}

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	protected void finalize() throws IOException {
        listening_socket.close();
    }
}